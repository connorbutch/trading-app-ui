# trading-app-ui

This project contains the ui for the trading application.  

To run this project directly with angular, use "ng serve -o"

In the future, this should be ran with docker (and docker compose for integration tests)

To run with Docker (the preferred way), do the following:
docker build . -t connorbutch/trading-app-ui:latest
docker run --name trading-app-ui -p 8080:80 -d connorbutch/trading-app-ui:latest
then go to http://localhost:8080/ and you will see your application running
